<?php

namespace EPro\Assignment\Application;

use EPro\Sender\Sender;

class Assignment
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * Assignment constructor
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Store assignment data
     *
     * @param array $data
     * @param string $token
     */
    public function store(array $data, string $token)
    {
        $result = $this->sender->sendJSON("POST", env("API_AUTH_URL") . "/teacher/test", ["token" => $token], $data, []);
        return json_decode($result->getBody());
    }

    /**
     * Store assignment data
     *
     * @param string $test_id
     * @param array $data
     * @param string $token
     */
    public function update(string $test_id, array $data, string $token)
    {
        $result = $this->sender->sendJSON("PUT", env("API_AUTH_URL") . "/teacher/test/$test_id", ["token" => $token], $data, []);
        return json_decode($result->getBody());
    }

    /**
     * Submit assignment
     * 
     * @param string $test_id
     * @param string $question_id
     * @param string $answer
     * @param string $token
     */
    public function submit(string $test_id, string $question_id, string $answer, string $token)
    {
        $result = $this->sender->sendJSON("POST", env("API_AUTH_URL") . "/student/test/submit", ["token" => $token], [
            "assignment_id" => $test_id,
            "question_id" => $question_id,
            "answer" => $answer,
        ], []);
        return json_decode($result->getBody());
    }
}
