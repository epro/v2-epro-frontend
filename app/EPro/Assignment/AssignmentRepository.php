<?php

namespace EPro\Assignment;

use EPro\Sender\Sender;

class AssignmentRepository
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * AssignmentRepository constructor
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Request list assignment to API
     * based on user role and
     * some additional parameters
     *
     * @param string $role
     * @param string $token
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    function list(string $role, string $token) {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/$role/test", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }

    function listResult(string $role, string $token) {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/$role/test/result", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }

    /**
     * Get list assignment for admin
     *
     * @param string $token
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function admin(string $token)
    {
        return $this->list("admin", $token);
    }

    public function adminResult(string $token)
    {
        return $this->listResult("admin", $token);
    }

    /**
     * Get list assignment for teacher
     *
     * @param string $token
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function teacher($token)
    {
        return $this->list("teacher", $token);
    }

    /**
     * Get list assignment for student
     *
     * @param string $token
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function student($token)
    {
        return $this->list("student", $token);
    }

    /**
     * Detail assignment
     *
     * @param string $assignment_id
     * @param string $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function detail(string $assignment_id, string $token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/teacher/test/$assignment_id", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }

    /**
     * Take assignment
     * 
     * @param string $assignment_id
     * @param string $question_id
     * @param string $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function takeAssignment($assignment_id, $question_id, $token)
    {
        $result = $this->sender->sendJSON("POST", env("API_AUTH_URL") . "/student/test/take", ["token" => $token], [
            "assignment_id" => $assignment_id,
            "question_id" => $question_id,
        ], []);
        return json_decode($result->getBody());
    }
}
