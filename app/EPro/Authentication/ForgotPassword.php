<?php

namespace EPro\Authentication;

use GuzzleHttp\Client;

class ForgotPassword
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * ForgotPassword constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    public function requestLink()
    {

    }

    public function resetPassword()
    {

    }
}