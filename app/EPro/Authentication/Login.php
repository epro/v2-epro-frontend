<?php

namespace EPro\Authentication;

use EPro\Sender\Sender;

/**
 * Class Login
 * @package EPro\Authentication
 */
class Login
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * Login constructor.
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Do login request
     *
     * @param $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function login($data)
    {
        $result = $this->sender->sendJSON("POST", env("API_AUTH_URL") . "/auth/login", [], $data, []);
        $result = json_decode($result->getBody());
        return $result;
    }
}