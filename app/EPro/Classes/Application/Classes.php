<?php

namespace EPro\Classes\Application;

use EPro\Sender\Sender;

class Classes
{

    /**
     * @param Sender
     */
    protected $sender;

    /**
     * Classes constructor
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Store class to API | administrator only
     *
     * @param $data
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store(array $data, string $token)
    {
        $result = $this->sender->sendJSON("POST", env("API_AUTH_URL") . "/admin/class", ["token" => $token], $data, []);
        return json_decode($result->getBody());
    }

    /**
     * Update class to API | administrator only
     *
     * @param $class_id
     * @param $data
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(string $class_id, array $data, string $token) {
        $result = $this->sender->sendJSON("PUT", env("API_AUTH_URL") . "/admin/class/$class_id", ["token" => $token], $data, []);
        return json_decode($result->getBody());
    }

    /**
     * Join new class | student only
     *
     * @param string $code
     * @param string $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function join(string $code, string $token)
    {
        $result = $this->sender->sendJSON("POST", env("API_AUTH_URL") . "/student/class/member/join", ["token" => $token], ["code" => $code], []);
        return json_decode($result->getBody());
    }
}