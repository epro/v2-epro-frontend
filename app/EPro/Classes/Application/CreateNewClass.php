<?php

namespace EPro\Classes\Application;

use EPro\Sender\Sender;

/**
 * Class CreateNewClass
 * @package Epro\Classes\Application
 */
class CreateNewClass
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * CreateNewClass constructor.
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Store class to API | admin only
     *
     * @param $data
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store($data, $token)
    {
        $result = $this->sender->sendJSON("POST", env("API_AUTH_URL") . "/admin/class", ["token" => $token], $data, []);
        return json_decode($result->getBody());
    }
}