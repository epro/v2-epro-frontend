<?php

namespace EPro\Classes\Application;

use EPro\Sender\Sender;

class DeleteClass
{

    /**
     * @param Sender
     */
    protected $sender;

    /**
     * Classes constructor
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    public function delete(string $class_id, string $token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/admin/class/delete/$class_id", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }


}

