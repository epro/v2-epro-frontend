<?php

namespace EPro\Classes;

use EPro\Sender\Sender;

/**
 * Class ClassRepository
 * @package EPro\Classes
 */
class ClassRepository
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * ClassRepository constructor.
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Get list classes for admin user
     *
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function admin($token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/admin/class", [
            "token" => $token
        ], [], []);
        return json_decode($result->getBody());
    }

    /**
     * Get list teacher's classes
     *
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function teacher($token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/teacher/class", [
            "token" => $token
        ], [], []);
        return json_decode($result->getBody());
    }

    /**
     * Get list student's classes
     *
     * @param string $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function student(string $token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/student/class", [
            "token" => $token
        ], [], []);
        return json_decode($result->getBody());
    }

    /**
     * Get detail class
     *
     * @param $token
     * @param $role
     * @param $class_idq
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function detail($token, $role, $class_id)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/$role/class/$class_id", [
            "token" => $token
        ], [], []);
        return json_decode($result->getBody());
    }

    public function member($token, $role, $class_id)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/$role/class/member/$class_id", [
            "token" => $token
        ], [], []);
        return json_decode($result->getBody());
    }
}