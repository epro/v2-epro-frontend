<?php

namespace EPro\Material\Application;

use EPro\Sender\Sender;

class Material
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * Assignment constructor
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Store material
     *
     * @param array $data
     * @param string $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store(array $data, string $token)
    {
        $result = $this->sender->sendJSON("POST", env("API_AUTH_URL") . "/teacher/material", ["token" => $token], $data, []);
        return json_decode($result->getBody());
    }

    /**
     * Update material
     *
     * @param array $data
     * @param string $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(array $data, string $material_id, string $token)
    {
        $result = $this->sender->sendJSON("PUT", env("API_AUTH_URL") . "/teacher/material/" . $material_id, ["token" => $token], $data, []);
        return json_decode($result->getBody());
    }
}
