<?php

namespace EPro\Material;

use EPro\Sender\Sender;

class MaterialRepository
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * ClassRepository constructor.
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Get list material teacher
     *
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list($token, $role, $class_id = null) {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/" . strtolower($role) . "/material", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }

    /**
     * list materi admin
     * @param $token
     * @param $role
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function listMaterial($token, $role) {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/" . strtolower($role) . "/material/materi", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }

    /**
     * Get detail material
     *
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function show(string $token, string $role, string $material_id)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/" . strtolower($role) . "/material/" . $material_id, ["token" => $token], [], []);
        return json_decode($result->getBody());
    }

    public function listMateri($token, $role, $class_id) {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/" . strtolower($role) . "/material/all/$class_id", ["token" => $token], [], []);
//        echo json_decode($result);
        return json_decode($result->getBody());
    }
}