<?php

namespace EPro;

use EPro\Sender\Sender;

class Profile
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * Profile constructor.
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Get detail profile
     *
     * @param $token
     * @param $role
     * @return \GuzzleHttp\Psr7\Response|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function detail($token, $role)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/" . $role . "/profile", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }

    /**
     * Update profile
     *
     * @param $data
     * @return \GuzzleHttp\Psr7\Response|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update($data)
    {
        $multipart = [[
            "name" => "data",
            "contents" => json_encode($data->except("_token")),
            "headers" => ["content-type" => "application/json"]
        ]];

        if ($data->file("inputImg") !== null) array_push($multipart, [
            "name" => "image",
            "filename" => $data->file("inputImg")->getClientOriginalName(),
            "contents" => fopen($data->file("inputImg")->path(), "r")
        ]);

        $result = $this->sender->sendMultipart("POST", env("API_AUTH_URL") . "/update", ["token" => $data->session()->get("token")], $multipart, $data->except("_token"), []);
        $result = json_decode($result->getBody());
        return $result;
    }
}