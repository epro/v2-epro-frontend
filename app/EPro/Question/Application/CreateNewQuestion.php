<?php

namespace EPro\Question\Application;

use EPro\Sender\Sender;

/**
 * Class CreateUser
 * @package EPro\Question\Application
 */
class CreateNewQuestion
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * CreateNewQuestion constructor
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Store question
     * 
     * @param string $assignment_id
     * @param array $questions
     * @param string $token
     * @return object
     * @throws GuzzleException
     */
    public function store(string $assignment_id, array $questions, string $token)
    {
        foreach ($questions as $key => $value) {
            $questions[$key]->assignment_id = $assignment_id;
            if ($questions[$key]->question_type === "MultipleChoice") {
                $questions[$key]->option = json_encode($value->options);
            }
        }

        $result = $this->sender->sendJSON("POST", env("API_AUTH_URL") . "/teacher/question", ["token" => $token], $questions, []);
        return json_decode($result->getBody());
    }
}
