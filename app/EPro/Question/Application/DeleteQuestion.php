<?php

namespace EPro\Question\Application;

use EPro\Sender\Sender;

/**
 * class DeleteQuestion
 * @package Epro\Question\Application
 */
class DeleteQuestion
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * DeleteQuestion constructor
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Delete question
     *
     * @param string $assignment_id
     * @param string $question_id
     * @param string $token
     * @return object
     * @throws GuzzleException
     */
    public function delete(string $assignment_id, string $question_id, string $token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/teacher/question/$assignment_id/$question_id/delete", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }
}
