<?php

namespace EPro\Question\Application;

use EPro\Sender\Sender;

/**
 * class UpdateQuestion
 * @package EPro\Question\Application
 */
class UpdateQuestion
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * UpdateQuestion constructor
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Update question
     * 
     * @param string $assignment_id
     * @param string $question_id
     * @param array $data
     * @param string $token
     * @return object
     * @throws GuzzleException
     */
    public function update(string $assignment_id, string $question_id, array $data, string $token)
    {
        $result = $this->sender->sendJSON("PUT", env("API_AUTH_URL") . "/teacher/question/$assignment_id/$question_id", ["token" => $token], $data, []);
        return json_decode($result->getBody());
    }
}