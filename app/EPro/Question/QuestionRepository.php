<?php

namespace EPro\Question;

use EPro\Sender\Sender;

/**
 * class QuestionRepository
 * @package EPro\Question
 */
class QuestionRepository
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * QuestionRepository constructor
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Request question data by assignment_id to API
     *
     * @param string $assignment_id
     * @param string $token
     * @return object
     * @throws GuzzleException
     */
    private function get(string $assignment_id, string $token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/teacher/question/$assignment_id", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }

    /**
     * Get list assignment for admin
     *
     * @param string $assignment_id
     * @param string $token
     * @return object
     * @throws GuzzleException
     */
    public function admin(string $assignment_id, string $token)
    {
        return $this->get($assignment_id, $token);
    }

    /**
     * Get list assignment for teacher
     *
     * @param string $assignment_id
     * @param string $token
     * @return object
     * @throws GuzzleException
     */
    public function teacher(string $assignment_id, string $token)
    {
        return $this->get($assignment_id, $token);
    }

    /**
     * Get list assignment for student
     *
     * @param string $assignment_id
     * @param string $token
     * @return object
     * @throws GuzzleException
     */
    public function student(string $assignment_id, string $token)
    {
        return $this->get($assignment_id, $token);
    }

    /**
     * Get detail assignment
     *
     * @param string $assignment_id
     * @param string $question_id
     * @param string $token
     * @return object
     * @throws GuzzleException
     */
    public function detail(string $assignment_id, string $question_id, string $token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/teacher/question/$assignment_id/$question_id", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }


}
