<?php

namespace EPro\Sender;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

/**
 * Class Sender
 * @package EPro\Sender
 */
class Sender
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * Sender constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Send request with JSON format
     *
     * @param string $method
     * @param string $url
     * @param array $headers
     * @param array $data
     * @param array $query
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendJSON(string $method, string $url, array $headers, array $data, array $query): Response
    {
        return $this->client->request($method, $url, [
            "headers" => $headers,
            "query" => $query,
            "json" => $data,
        ]);
    }

    /**
     * Send request with multipart form-data (files, images, etc.)
     *
     * @param string $method
     * @param string $url
     * @param array $headers
     * @param mixed $data
     * @param array $json_data
     * @param string $query
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendMultipart(string $method, string $url, array $headers, $data, array $json_data, string $query): Response
    {
        return $this->client->request($method, $url, [
            "headers" => $headers,
            "query" => $query,
            "multipart" => $data
        ]);
    }
}