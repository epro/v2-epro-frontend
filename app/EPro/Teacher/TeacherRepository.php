<?php

namespace EPro\Teacher;

use EPro\Sender\Sender;

/**
 * Class TeacherRepository
 * @package EPro\Teacher
 */
class TeacherRepository
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * TeacherRepository constructor.
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Get list teacher
     *
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list($token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/teacher", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }

    /**
     * Search teacher by name
     *
     * @param $search_key
     * @param $tokenph
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function search($search_key, $token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/teacher", ["token" => $token], [], ["q" => $search_key]);
        return json_decode($result->getBody());
    }

    public function detail($id)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/teacher/" . $id, [], [], []);
        return json_decode($result->getBody());
    }
}