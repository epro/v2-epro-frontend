<?php

namespace EPro\User\Application;

use EPro\Sender\Sender;

/**
 * Class CreateUser
 * @package EPro\User\Application
 */
class CreateUser
{

    /**
     * @var Sender
     */
    protected $sender;

    /**
     * CreateUser constructor.
     */
    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Store user to API
     *
     * @param $data
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store($data, $token)
    {
        $result = $this->sender->sendJSON("POST", env("API_AUTH_URL") . "/auth/register", ["token" => $token], $data, []);
        return json_decode($result->getBody());
    }
}