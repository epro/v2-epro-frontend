<?php

namespace EPro\User;

use EPro\Sender\Sender;

class UserRepository
{

    /**
     * @var Sender
     */
    protected $sender;


    public function __construct()
    {
        $this->sender = new Sender();
    }

    /**
     * Get list student
     *
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list($token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/student/all", ["token" => $token], [], []);
        return json_decode($result->getBody());
    }

    /**
     * Search student by name
     *
     * @param $search_key
     * @param $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function search($search_key, $token)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/student/all", ["token" => $token], [], ["q" => $search_key]);
        return json_decode($result->getBody());
    }

    public function detail($token, $role, $student_id)
    {
        $result = $this->sender->sendJSON("GET", env("API_AUTH_URL") . "/$role/student/$student_id", [
            "token" => $token
        ], [], []);
        return json_decode($result->getBody());
    }
}