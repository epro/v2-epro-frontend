<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use EPro\Assignment\Application\Assignment;
use EPro\Assignment\AssignmentRepository;
use Illuminate\Http\Request;

class AssignmentController extends Controller
{

    /**
     * @var Assignment
     */
    protected $assignment;

    /**
     * @var AssignmentRepository
     */
    protected $assignmentRepository;

    /**
     * AssignmentController constructor
     */
    public function __construct()
    {
        $this->assignment = new Assignment();
        $this->assignmentRepository = new AssignmentRepository();
    }

    /**
     * List assignment
     */
    public function index(Request $request)
    {
        try {
            $assignments = $this->assignmentRepository->admin($request->session()->get("token"));
            return view("admin.soal.list-soal", [
                "assignments" => $assignments->data
            ]);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function show(Request $request)
    {
        try {
            $assignments = $this->assignmentRepository->adminResult($request->session()->get("token"));
            return view("admin.test.list-test", [
                "assignments" => $assignments->data
            ]);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

}
