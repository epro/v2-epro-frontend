<?php

namespace App\Http\Controllers\Administrator;

use EPro\Classes\Application\DeleteClass;
use Illuminate\Http\Request;
use EPro\Classes\ClassRepository;
use App\Http\Controllers\Controller;
use EPro\Classes\Application\Classes;
use App\Http\Requests\Classes\StoreClass;
use EPro\Classes\Application\CreateNewClass;

class ClassesController extends Controller
{

    protected $classes;

    protected $classesRepository;

    public function __construct()
    {
        $this->classes = new Classes();
        $this->classesRepository = new ClassRepository();
    }

    /**
     * List classes
     */
    public function index(Request $request)
    {
        try {
            $classes = $this->classesRepository->admin($request->session()->get("token"));
            return view("admin.classes.list-classes", [
                "classes" => $classes->data
            ]);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * Detail class by id
     */
    public function show($class_id, Request $request)
    {
        try {
            $result = $this->classesRepository->detail($request->session()->get("token"), strtolower($request->session()->get("role")), $class_id);
            $member = $this->classesRepository->member($request->session()->get("token"), strtolower($request->session()->get("role")), $class_id);
            return view("admin.classes.detail-class", [
                "class" => $result->data,
                "member" =>$member->data
            ]);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * Edit class
     */
    public function edit($class_id, Request $request) {
        try {
            $result = $this->classesRepository->detail($request->session()->get("token"), strtolower($request->session()->get("role")), $class_id);
            return view("admin.classes.edit-class", (array)$result->data);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * Update class
     */
    public function update($class_id, Request $request) {
        try {
            $this->classes->update($class_id, $request->except("_token"), $request->session()->get("token"));
            return redirect()->route("admin.classes.show", ["id" => $class_id]);
        } catch(\Exception $exception) {
            dd($exception);
        }
    }

    public function create()
    {
        try {
            return view("admin.classes.add-class");
        } catch (\Exception $exception) {
            flash("Terjadi kesalahan!")->error()->important();
            return redirect()->back();
        }
    }

    public function store(StoreClass $request)
    {
        try {
            $createNewClass = new CreateNewClass();
            $createNewClass->store($request->except("_token"), $request->session()->get("token"));
            return redirect()->route("admin.classes.index");
        } catch (\Exception $exception) {
            dd($exception);
            flash("Terjadi kesalahan!")->error()->important();
            return redirect()->back();
        }
    }

    public function destroy($class_id, Request $request)
    {
        try {
            $deleteClass = new DeleteClass();
            $deleteClass->delete($class_id, $request->session()->get("token"));
            return redirect()->back();
        } catch (\Exception $exception) {
            dd($exception);
        }
    }


}