<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use EPro\Material\Application\Material;
use EPro\Material\MaterialRepository;

class MaterialController extends Controller
{
    protected $material;

    protected $materialRepository;

    public function __construct(Material $material, MaterialRepository $materialRepository)
    {
        $this->material = $material;
        $this->materialRepository = $materialRepository;
    }

    public function index(Request $request)
    {
        try {
            $material = $this->materialRepository->listMaterial($request->session()->get("token"), $request->session()->get("role"));
            return view("admin.materi.list-materi", [
                "material" => $material->data,
            ]);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

}