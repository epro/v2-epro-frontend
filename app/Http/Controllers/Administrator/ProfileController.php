<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Administrator
 */
class ProfileController extends Controller
{

    /**
     * @var Request
     */
    protected $request;

    /**
     * ProfileController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function show()
    {
        try {

        } catch (\Exception $exception) {

        }
    }

    public function update()
    {
        try {

        } catch (\Exception $exception) {

        }
    }
}