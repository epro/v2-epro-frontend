<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUser;
use EPro\Student\StudentRepository;
use EPro\User\Application\CreateUser;
use Illuminate\Http\Request;

class StudentController extends Controller
{

    protected $studentRepository;

    public function __construct()
    {
        $this->studentRepository = new StudentRepository();
    }

    public function index(Request $request)
    {
        try {
            $result = $this->studentRepository->list($request->session()->get("token"));
            return view("admin.student.list-student", [
                "students" => $result->data
            ]);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    public function show()
    {
        try {

        } catch (\Exception $exception) {

        }
    }

    public function create()
    {
        try {
            return view("admin.student.add-student");
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function store(StoreUser $request)
    {
        try {
            $createUser = new CreateUser();
            $request["role"] = "STUDENT";
            $createUser->store($request->except("_token"), $request->session()->get("token"));
            return redirect()->route("admin.student.index");
        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    /**
     * Edit class
     */
    public function edit($student_id, Request $request) {
        try {
            $result = $this->studentRepository->detail($request->session()->get("token"), strtolower($request->session()->get("role")), $student_id);
            return view("admin.student.edit-class", (array)$result->data);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * Update class
     */
    public function update($class_id, Request $request) {
        try {
            $this->classes->update($class_id, $request->except("_token"), $request->session()->get("token"));
            return redirect()->route("admin.classes.show", ["id" => $class_id]);
        } catch(\Exception $exception) {
            dd($exception);
        }
    }
}