<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUser;
use EPro\Teacher\TeacherRepository;
use EPro\User\Application\CreateUser;
use Illuminate\Http\Request;

class TeacherController extends Controller
{

    protected $teacherRepositoy;

    public function __construct()
    {
        $this->teacherRepositoy = new TeacherRepository();
    }

    public function index(Request $request)
    {
        try {
            $result = $this->teacherRepositoy->list($request->session()->get("token"));
            return view("admin.teacher.list-teacher", [
                "teachers" => $result->data
            ]);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    public function show($id)
    {
        try {
            $result = $this->teacherRepositoy->detail($id);
            dd($result);
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function create()
    {
        try {
            return view("admin.teacher.add-teacher");
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function store(StoreUser $request)
    {
        try {
            $createUser = new CreateUser();
            $request["role"] = "TEACHER";
            $createUser->store($request->except("_token"), $request->session()->get("token"));
            flash("Sucessfully add new teacher!")->important()->success();
            return redirect()->route("admin.teacher.index");
        } catch (\Exception $exception) {
            flash($exception->getMessage())->error()->important();
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try {

        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    public function update($id, Request $request)
    {
        try {

        } catch (\Exception $exception) {
            dd($exception);
        }
    }
}