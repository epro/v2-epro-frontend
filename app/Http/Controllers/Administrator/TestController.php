<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function index()
    {
        try {
            return view("admin.test.list-test");
        } catch (\Exception $exception) {
            dd($exception);
        }
    }
}