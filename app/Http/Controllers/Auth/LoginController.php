<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use EPro\Authentication\Login;

class LoginController extends Controller
{

    public function index ()
    {
        try {
            return view("login");
        } catch (\Exception $exception) {
            dd("Terjadi error, kapok! wkwkkw");
        }
    }

    public function login (LoginRequest $request)
    {
        try {
            $login = new Login();
            $result = $login->login($request->all());

            /**
             * Set session
             */
            $request->session()->put((array)$result->data);

            /**
             * Redirect to the routes based on user role
             */
            return redirect(route(strtolower($result->data->role) . ".dashboard"));
        } catch (\Exception $exception) {
            flash("An error occurred while logging in, please contact your administrator!")->error()->important();
            return redirect()->back();
        }
    }
}