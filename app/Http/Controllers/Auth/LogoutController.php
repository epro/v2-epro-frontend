<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class LogoutController
 * @package EPro\Authentication
 */
class LogoutController extends Controller
{

    /**
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $request->session()->flush();
        return redirect("/");
    }
}