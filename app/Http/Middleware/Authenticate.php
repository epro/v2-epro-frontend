<?php

namespace App\Http\Middleware;

/**
 * Class Authenticate
 * @package App\Http\Middleware
 */
class Authenticate
{

    /**
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handle($request)
    {
        if ($request->session()->get("token") !== null) {
            return redirect()->route(strtolower($request->session()->get("role")) . ".dashboard");
        } else {
            return redirect()->route("login.index");
        }
    }
}