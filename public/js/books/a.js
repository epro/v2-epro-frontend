$(function() {
	// Cara 1
	$.ajax({
		url: 'localhost:1234/getData', // Server address
		type: 'GET', // Jenis request GET, POST, PUT, dll.
		dataType: 'json', // Tipe / format response data,
		done: function(response) { // Response sukses
			// Aksi setelah response sukses
			console.log(response);
		},
		fail: function(errResponse) { // Respon gagal
			// Aksi setelah respon gagal
		}
	});

	// Cara 2 => Assign ajax to a variable
	// Variable => 3: const, let, var
	// let => Pendeklarasian variabel yang value nya bisa diubah
	// const => Pendeklarasian variabel yang tidak bisa diubah value nya
	// var => Paling lemah
	let request = $.ajax({
		const biodata = {
			name: 'AISHCU'
		};

		url: 'localhost:1234/getData', // Server address
		type: 'POST', // Jenis request GET, POST, PUT, dll.
		dataType: 'json', // Tipe / format response data,
		data: biodata
	});

	request.done(function(response) {
		console.log(response);
	});

	request.fail(function(errResponse) => {
		console.log(errResponse);
	});
});