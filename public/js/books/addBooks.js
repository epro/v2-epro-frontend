$(function() {
	// Set autofocus and reset input modals
	$('#addCategory').on('shown.bs.modal', function() {
		$('#categoryName').focus();
	});

	$('#addCategory').on('hide.bs.modal', function() {
		$('#categoryName').val('');
	});

	$('#addSubcategory').on('shown.bs.modal', function() {
		$('#subcategoryName').focus();
	});

	$('#addSubategory').on('hide.bs.modal', function() {
		$('#subcategoryName').val('');
	});

	$('#addPenerbit').on('shown.bs.modal', function() {
		$('#penerbitName').focus();
	});

	$('#addPenerbit').on('hide.bs.modal', function() {
		$('#penerbitName').val('');
		$('#penerbitAddress').val('');
		$('#penerbitPhone').val('');
		$('#penerbitEmail').val('');
	});

	// Load data category and penerbit
	$(window).load(function() {
		let request = $.ajax({
			url: '/books/add/prepare',
			type: 'get'
		});

		request.done(function(response) {
			let data = JSON.parse(response);
			$('#selectBookCategory').find('option').remove().end().append('<option></option>');
			$.each(data.categories, function(key, value) {
				$('#selectBookCategory').append($("<option></option>").attr("value", value.id).text(value.name));
			});

			$('#selectPenerbit').find('option').remove().end().append('<option></option>');
			$.each(data.penerbit, function(key, value) {
				$('#selectPenerbit').append($("<option></option>").attr("value", value.id).text(value.name));
			});
		});

		request.fail(function(errResponse) {
			console.log(errResponse);
		});
	});

	// Add new category
	$('#saveCategory').click(function() {
		let data = {
			categoryName: $('#categoryName').val()
		};

		let request = $.ajax({
			url: '/category/add',
			type: 'post',
			data: data
		});

		request.done(function(response) {
			$('#categoryName').val(null);
			$('#addCategory').modal('toggle');

			$('#selectBookCategory').find('option').remove().end().append('<option></option>');

			let data = JSON.parse(response);
			$.each(data, function(key, value) {
				$('#selectBookCategory').append($("<option></option>").attr("value", value.id).text(value.name));
			});
		});

		request.fail(function(errResponse) {
			console.log(errResponse);
		});
	});

	// Get Subcategory by categoryId
	$('#selectBookCategory').change(function() {
		let categoryId = $(this).val();

		if (categoryId != '' || categoryId != undefined || categoryId != null) {
			let request = $.ajax({
				url: '/category/sub?categoryId=' + categoryId,
				method: 'get'
			});

			request.done(function(response) {
				$('#selectBookSubcategory').find('option').remove().end().append('<option></option>');
				let data = JSON.parse(response);
				$.each(data, function(key, value) {
					$('#selectBookSubcategory').append($("<option></option>").attr("value", value.id).text(value.name));
				});
			});

			request.fail(function(errResponse) {
				console.log(JSON.parse(errResponse));
			});
		}
	});

	// Add new Subcateory
	$('#saveSubcategory').click(function() {
		let categoryId = $('#selectBookCategory').val();
		let name = $('#subcategoryName').val();

		let data = {
			name: name,
			categoryId: categoryId
		};

		let request = $.ajax({
			url: '/subcategory/add',
			type: 'post',
			data: data
		});

		request.done(function(response) {
			$('#subcategoryName').val('');
			$('#addSubcategory').modal('toggle');

			$('#selectBookSubcategory').find('option').remove().end().append('<option></option>');
			data = JSON.parse(response);
			$.each(data, function(key, value) {
				$('#selectBookSubcategory').append($("<option></option>").attr("value", value.id).text(value.name));
			});
		});

		request.fail(function(errResponse) {
			console.log(JSON.parse(errResponse));
		});
	});

	// Add Penerbit
	$("#savePenerbit").click(function() {
		let data = {
			name: $("#penerbitName").val(),
			address: $("#penerbitAddress").val(),
			email: $("#penerbitEmail").val(),
			phone: $("#penerbitPhone").val()
		};

		let request = $.ajax({
			url: '/penerbit/add',
			type: 'post',
			data: data
		});

		request.done(function(response) {
			$('#penerbitName').val('');
			$('#penerbitAddress').val('');
			$('#penerbitPhone').val('');
			$('#penerbitEmail').val('');
			$("#addPenerbit").modal('toggle');

			$('#selectPenerbit').find('option').remove().end().append('<option></option>');
			data = JSON.parse(response);
			$.each(data, function(key, value) {
				$('#selectPenerbit').append($("<option></option>").attr("value", value.id).text(value.name));
			});
		});

		request.fail(function(errResponse) {
			console.log(errResponse);
		});
	});

	// Show image preview
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imgPreview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
     
    $("#inputImg").change(function(){
        readURL(this);
    });
});