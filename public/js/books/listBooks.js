$(function() {
	$(window).load(function() {
		let request = $.ajax({
			url: '/books/all',
			type: 'get'
		});

		request.success(function(response) {
			let data = JSON.parse(response);

			$("#txTotalProduk").text(data.length);
		});

		request.fail(function(errResponse) {
			console.log(errResponse);
		});
	});
	
	$('[data-toggle="tooltip"]').tooltip();

	$('[id="bookImgs"]').click(function() {
		$('#theImg').attr('src', $(this).children().children().attr('src'));
	});
});