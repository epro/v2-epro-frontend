@extends('layout.app')

@section('content')
	<div id="content" class="app-content" role="main">
		<div class="app-content-body">
			<div class="bg-light lter b-b wrapper-md">
				<div class="row">
					<div class="col-md-4 col-sm-12 col-xs-12" style="padding-bottom:5px;">
						<h1 class="m-n font-thin h3">Add teacher</h1>
						<i class="text-muted text-sm">Here you can add new teacher data</i>
					</div>
				</div>
			</div>

			<div class="wrapper-md" id="addbooks">
				<div class="row">
					<div class="col-md-3" style="padding: 16px">
						<h3>Add Teacher</h3>
			            <br>
			            <p style="text-align: justify">
			                <strong>Instruction</strong> : <br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			            </p>
					</div>
					<div class="col-md-9">
						@include('flash::message')
						<div class="card">
							<div class="body">
                    			<div class="card-inner">
                        			<h4 class="card-inner-header">Data Pengajar</h4>

                        			<form method="POST" action="{{ route("admin.teacher.store") }}">

		                                {{ csrf_field() }}

		                                <div class="form-group">
		                                    <label for="class_name" class="config-label">Name :</label>
		                                    <input id="class_name" type="text" class="form-control" name="name" required/>
		                                </div>
		                                <div class="form-group">
		                                    <label for="class_name" class="config-label">Email :</label>
		                                    <input id="class_name" type="email" class="form-control" name="email" required/>
		                                </div>
		                                <div class="form-group">
		                                    <label for="class_name" class="config-label">Phone Number :</label>
		                                    <input id="class_name" type="text" class="form-control" name="phone" required/>
		                                </div>
		                                <div class="form-group">
		                                    <label for="class_name" class="config-label">Username :</label>
		                                    <input id="class_name" type="text" class="form-control" name="username" required/>
		                                </div>
		                                <div class="row">
		                                    <div class="col-md-6">
		                                        <div class="form-group">
		                                            <label for="class_name" class="config-label">Password :</label>
		                                            <input id="class_name" type="password" class="form-control" name="password" required/>
		                                        </div>
		                                    </div>
		                                    <div class="col-md-6">
		                                        <div class="form-group">
		                                            <label for="class_name" class="config-label">Password Confirmation :</label>
		                                            <input id="class_name" type="password" class="form-control" name="password_confirmation" required/>
		                                        </div>
		                                    </div>
		                                </div>
		                                <button class="btn btn-danger waves-effect" type="reset"><em class="fa fa-times"></em>
		                                    Cancel
		                                </button>
		                                <button class="btn btn-primary waves-effect" type="submit"><em class="fa fa-check"></em>
		                                    Submit
		                                </button>
		                            </form>
                        		</div>
                        	</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript">
		$("input[type=text]").attr("autocomplete", "off");
	</script>
@endpush