@extends('layout.app')

@push('title')
	<title>E-Pro | List Teacher</title>
@endpush

@push('styles')
	<style type="text/css">
		.table > tbody > tr > td {
            vertical-align: middle;
        }

        .table > thead > tr > th {
            font-weight: bold;
        }

        .table > tbody > tr > td:nth-child(1) {
            font-weight: bold;
        }
	</style>
@endpush

@section('content')
	<div id="content" class="app-content" role="main">
		<div class="app-content-body">
			<div class="bg-light lter b-b wrapper-md">
				<div class="row">
					<div class="col-md-4 col-sm-12 col-xs-12" style="padding-bottom:5px;">
						<h1 class="m-n font-thin h3">List teacher</h1>
						<i class="text-muted text-sm">Here you can manage teachers data</i>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-6">
						<div class="text-primary-dk font-thin h3"><span id="txTotalProduk">{{ count($teachers) }}</span></div>
          				<i class="text-muted text-sm">Teacher Total</i>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-4" align="right">
						<a href="{{ route('admin.teacher.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Teacher</a>
					</div>
				</div>
			</div>

			<div class="wrapper-md" id="addbooks" style="background-color: white;">
				@include('flash::message')
				<div class="panel panel-default" style="padding: 10px; margin:-17px; border:0px; box-shadow:none;">
					<div class="panel-body b-b b-light">
				      	Teacher Look up: <input id="filter" type="text" class="form-control input-sm w-sm inline m-r"/>
				    </div>
				    <div>
				    	<table class="table m-b-none" ui-jq="footable" data-filter="#filter" id="tabelBooks" data-page-size="10">
							<thead class="bg-primary">
								<tr>
									<th>#</th>
									<th>NAME</th>
									<th data-hide="phone,tablet" data-name="E-mail">E-MAIL</th>
									<th data-hide="phone,tablet" data-name="Phone">PHONE NUMBER</th>
									<th data-hide="phone" data-name="Manage">MANAGE</th>
								</tr>
							</thead>
							<tbody>
								@forelse ($teachers as $teacher)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{ $teacher->name }}</td>
										<td><a href="mailto:{{ $teacher->email }}">{{ $teacher->email }}</a></td>
										<td>{{ $teacher->phone }}</td>
										<td>
											<button type="button" class="btn btn-xs btn-primary" style="font-weight: bold;" disabled><em class="fa fa-eye"></em> Detail</button>
											<button type="button" class="btn btn-xs btn-warning" style="font-weight: bold;" disabled><em class="fa fa-pencil-square-o"></em> Edit</button>
											<button type="button" class="btn btn-xs btn-danger" style="font-weight: bold;" disabled><em class="fa fa-pencil-square-o"></em> Delete</button>
										</td>
									</tr>
								@empty
									<tr>
										<td colspan="4">Teacher data is empty</td>
									</tr>
								@endforelse
							</tbody>
							<tfoot>
					        	<tr>
					            	<td colspan="5" class="text-center">
					                	<ul class="pagination"></ul>
					              	</td>
					          	</tr>
					        </tfoot>
				    	</table>
				    </div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="imgPreview">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<center><img id="theImg" alt="" width="100%" style="max-height:350px;"></center>
				</div>
				<div class="modal-footer">
					<center><button class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Tutup</button></center>
				</div>
			</div>
		</div>
	</div>
@endsection