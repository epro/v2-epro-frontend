<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	@stack('title')
	<link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
	@stack('styles')
</head>
<body>
	<div class="app app-header-fixed">
		<header id="header" class="app-header navbar" role="menu">
			<div class="navbar-header bg-dark">
				<button class="pull-right visible-xs dk" ui-toggle="show" target=".navbar-collapse"><i class="glyphicon glyphicon-cog"></i></button>
				<button class="pull-right visible-xs" ui-toggle="off-screen" target=".app-aside" ui-scroll="app"><i class="glyphicon glyphicon-align-justify"></i></button>
				<a href="" class="navbar-brand text-lt">
			        <i class="fa fa-book" aria-hidden="true"></i>
			        <span class="hidden-folded m-l-xs">E-PRO</span>
			    </a>
			</div>
			<div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
				<div class="nav navbar-nav hidden-xs">
					<a href="" class="btn no-shadow navbar-btn" ui-toggle="app-aside-folded" target=".app">
			            <i class="fa fa-dedent fa-fw text"></i>
			            <i class="fa fa-indent fa-fw text-active"></i>
			        </a>
			        <a href="" class="btn no-shadow navbar-btn" ui-toggle="show" target="#aside-user">
			            <i class="icon-user fa-fw"></i>
			        </a>
				</div>
				<ul class="nav navbar-nav navbar-right">
          			<li class="dropdown">
			            <a href="" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
			              	<span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
			                	<img src="{{ asset('img/a0.jpg') }}" alt="...">
			                	<i class="on md b-white bottom"></i>
			              	</span>
			              	<span class="hidden-sm hidden-md">{{ session()->get('name') }}</span> <b class="caret"></b>
			            </a>
			            <ul class="dropdown-menu animated fadeInRight w">
			              	<li>
			                	<a data-toggle="modal" data-target="#modalLogout"><i class="fa fa-sign-out"></i> Log out</a>
			              	</li>
			            </ul>
          			</li>
        		</ul>
			</div>
		</header>

		@if (session()->get('role') === 'ADMIN')
			@include('layout.sidebar-admin')
		@elseif (session()->get('role') === 'STUDENT')
			@include('layout.sidebar-admin')
		@elseif (session()->get('role') === 'TEACHER')
			@include('layout.sidebar-admin')
		@endif

  		@yield('content')
	</div>

	<div class="modal fade" id="modalLogout">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body" align="center">
					<strong>Are you sure want to log out?</strong>
				</div>
				<div class="modal-footer">
					<center>
						<form action="{{ route('logout.index') }}" method="POST">
							{!! csrf_field() !!}
							<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> No</button>
							<button class="btn btn-danger" type="submit" name="confirmed"><i class="fa fa-sign-out"></i> Yes</button>
						</form>
					</center>
				</div>
			</div>
		</div>
	</div>

	<script src="{{ asset('plugins/jQuery/jquery.min.js') }}"></script>
	<script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
	<script src="{{ asset('js/ui-load.js') }}"></script>
	<script src="{{ asset('js/ui-jp.config.js') }}"></script>
	<script src="{{ asset('js/ui-jp.js') }}"></script>
	<script src="{{ asset('js/ui-nav.js') }}"></script>
	<script src="{{ asset('js/ui-toggle.js') }}"></script>
	@stack("scripts")
</body>
</html>