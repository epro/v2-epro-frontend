<aside id="aside" class="app-aside hidden-xs bg-dark">
	<br>
  	<div class="aside-wrap">
  		<nav ui-nav class="navi clearfix">
			<ul class="nav">
				<li><a href="" class="auto"><i class="fa fa-dashboard icon text-primary"></i><span class="font-bold">Dashboard</span></a></li>
				<li>
					<a href class="auto">
						<span class="pull-right text-muted">
							<i class="fa fa-fw fa-angle-right text"></i>
							<i class="fa fa-fw fa-angle-down text-active"></i>
						</span>
						<i class="fa fa-book text-success"></i>
						<span>Teacher</span>
					</a>
					<ul class="nav nav-sub dk">
						<li class="nav-sub-header"><a href><span>Teacher</span></a></li>
						<li><a href=""><span>Add Teacher</span></a></li>
						<li><a href="{{ route('admin.teacher.index') }}"><span>List Teacher</span></a></li>
					</ul>
				</li>
				{{-- <li>
					<a href class="auto">
						<span class="pull-right text-muted">
							<i class="fa fa-fw fa-angle-right text"></i>
							<i class="fa fa-fw fa-angle-down text-active"></i>
						</span>
						<i class="fa fa-users text-warning"></i>
						<span>Anggota</span>
					</a>
					<ul class="nav nav-sub dk">
						<li class="nav-sub-header"><a href><span>Anggota</span></a></li>
						<li><a href=""><span>Tambah Anggota</span></a></li>
						<li><a href=""><span>Daftar Anggota</span></a></li>
					</ul>
				</li>
				<li>
					<a href class="auto">
						<span class="pull-right text-muted">
							<i class="fa fa-fw fa-angle-right text"></i>
							<i class="fa fa-fw fa-angle-down text-active"></i>
						</span>
						<i class="fa fa-exchange text-danger"></i>
						<span>Pinjaman</span>
					</a>
					<ul class="nav nav-sub dk">
						<li class="nav-sub-header"><a href><span>Pinjaman</span></a></li>
						<li><a href=""><span>Tambah Pinjaman</span></a></li>
						<li><a href=""><span>Daftar Pinjaman</span></a></li>
					</ul>
				</li>
				<li><a href="" class="auto"><i class="glyphicon glyphicon-stats icon tet-primary-dker"></i><span>Laporan</span></a></li>
				<li><a href="" class="auto"><i class="fa fa-question-circle"></i><span>Bantuan</span></a></li> --}}
			</ul>
  		</nav>
	</div>
</aside>