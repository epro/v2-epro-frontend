<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Library | Login</title>
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
</head>
<body style="background: #eeeeee;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3" style="position: absolute; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%);">
                @include('flash::message')
                <div class="panel panel-default">
                    <div class="panel-body">
                        <center>
                          <h2>E-Pro | <small>E-learning PROBISTEK</small></h2>
                          <hr>
                        </center>
                        
                        <form name="form" method="POST" action="{{ route("login.login") }}">
                            
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="sr-only">Username</label>
                                <input type="text" name="username" class="form-control" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <label class="sr-only">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Password" required>
                            </div>
                            <hr>
                            <div class="text-center">
                                <center>
                                    <button type="submit" name="login" style="margin-top:-8px;" class="btn btn-sm btn-primary btn-block"><b>Masuk</b></button>
                                </center>
                            </div>

                            <div ng-show="err" class="form-group has-error">
                                <p class="help-block text-center">
                                    <br>
                                    <a href="" style="text-decoration:underline">Forgot password?</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('plugins/jQuery/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
</body>
</html>