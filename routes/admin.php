<?php

use Illuminate\Support\Facades\Route;

Route::middleware("web")->name("admin.")->group(function () {
    Route::namespace("Administrator")->group(function () {
        Route::get("dashboard", "DashboardController@index")->name("dashboard");

        /**
         * Teacher route resource
         */
        Route::resource("teacher", "TeacherController");

        /**
         * Class route resource
         */
        Route::resource("classes", "ClassesController");

        /**
         * Class route resource
         */
        Route::resource("materi", "MaterialController");

        /**
         * assignment
         */
        Route::resource("soal", "AssignmentController");


        /**
         * STudent route
         */
        Route::resource("student", "StudentController");

        Route::get("materi", "MaterialController@index")->name("materi.index");

        Route::get("test", "TestController@index")->name("test.index");

//        Route::get("soal", "SoalController@index")->name("soal.index");

    });

});