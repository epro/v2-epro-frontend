<?php

use Illuminate\Support\Facades\Route;

Route::middleware("web")->name("student.")->group(function () {
    Route::namespace("Student")->group(function () {
        Route::get("dashboard", "DashboardController@index")->name("dashboard");

        Route::resource("classes", "ClassesController");
        Route::post("classes/join", "ClassesController@join")->name("classes.join");

        Route::get("calendar", "ScheduleController@index")->name("calendar.index");

        Route::get("detail/material", "MaterialController@index")->name("material.index");

        Route::get("assignment", "AssignmentController@index")->name("assignment.index");
        Route::get("assignment/{assignment_id}/{question_id?}", "AssignmentController@takeAssignment")->name("assignment.take");
        Route::post("assignment/{assignment_id}/{question_id?}", "AssignmentController@submitAssignment")->name("assignment.submit");
    });

    /**
     * Profile routes
     */
    Route::get("profile", "ProfileController@index")->name("profile.index");
    Route::get("profile/edit", "ProfileController@edit")->name("profile.edit");
    Route::post("profile/update", "ProfileController@update")->name("profile.update");
});