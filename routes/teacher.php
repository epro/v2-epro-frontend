<?php

use Illuminate\Support\Facades\Route;

Route::middleware("web")->name("teacher.")->group(function () {
    Route::namespace ("Teacher")->group(function () {
        /**
         * Teacher dashboard route
         */
        Route::get("dashboard", "DashboardController@index")->name("dashboard");

        /**
         * Classes route resource
         */
        Route::resource("classes", "ClassesController");

        Route::get("calendar", "ScheduleController@index")->name("calendar.index");

        /**
         * Assignment route resource
         */
        Route::resource("assignment", "AssignmentController");

        Route::resource("material", "MaterialController");

        /**
         * Question route resource
         */
        Route::prefix("question")->name("question.")->group(function () {
            /**
             * Create
             */
            Route::post("/{assignment_id}", "QuestionController@store")->name("store");
            Route::get("/{assignment_id}/create", "QuestionController@create")->name("create");

            /**
             * Update
             */
            Route::get("/{assignment_id}/{question_id}/edit", "QuestionController@edit")->name("edit");
            Route::put("/{assignment_id}/{question_id}/update", "QuestionController@update")->name("update");

            /**
             * Delete
             */
            Route::get("/{assignment_id}/{question_id}/delete", "QuestionController@destroy")->name("destroy");
        });
    });

    /**
     * Profile routes
     */
    Route::get("profile", "ProfileController@index")->name("profile.index");
    Route::get("profile/edit", "ProfileController@edit")->name("profile.edit");
    Route::post("profile/update", "ProfileController@update")->name("profile.update");
});
