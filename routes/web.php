<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::middleware("home.authenticate")->group(function () {
    Route::get('/', function () {
        return view('welcome');
    });
});

Route::group([
    "middleware" => ["web"]
], function () {
    /**
     * Dashboard
     */
    Route::get("/dashboard", "Student\DashboardController@index")->name("dashboard");
    Route::get("/dashboard", "Teacher\DashboardController@index")->name("dashboard");
    Route::get("/dashboard", "Administrator\DashboardController@index")->name("dashboard");

    /**
     * Login
     */
    Route::get("/login", "Auth\LoginController@index")->name("login.index");
    Route::post("/login", "Auth\LoginController@login")->name("login.login");
});

Route::post("/logout", "Auth\LogoutController@index")->name("logout.index");